
import requests
from requests.exceptions import RequestException
import logging
import asyncio


# Function to make API requests with error handling
async def _make_api_request(url, api_key, params=None):
    try:
        headers = {"X-Api-Key": api_key}
        response = await asyncio.get_event_loop().run_in_executor(
            None, lambda: requests.get(url, params=params, headers=headers)
        )
        response.raise_for_status()
        return response.json()
    except RequestException as e:
        logging.error(f"Error making API request to {url}: {e}")
        return None
    except ValueError as e:
        logging.error(f"Error parsing JSON response from {url}: {e}")
        return None


# Make a request to view and count items in queue and return the number.
async def _count_queue_records(arr_api_url: str, arr_api_key: str):
    logging.debug(
        "Counting amount of elements in the queue of *arr instance " + arr_api_url
    )
    the_url = f"{arr_api_url}/queue"
    the_queue = await _make_api_request(the_url, arr_api_key)
    if the_queue is not None and "records" in the_queue:
        logging.debug("Found " + str(len(the_queue)) + " elements in the queue.")
        return the_queue["totalRecords"]



async def get_missing_elements_paths(
    arr_api_url: str, arr_api_key: str, arr_target_folder: str
) -> list[str]:
    logging.info("Discovering missing elements on *arr instance " + arr_api_url)
    arr_url = f"{arr_api_url}/queue"
    arr_queue = await _make_api_request(
        arr_url,
        arr_api_key,
        {"page": "1", "pageSize": await _count_queue_records(arr_api_url, arr_api_key)},
    )
    arr_queue_missing_files = [
        movie
        for movie in arr_queue["records"]
        if movie["trackedDownloadState"] == "importPending"
    ]
    queue_missing_files_paths: list[str] = []
    for missing_file in arr_queue_missing_files:
        path: str = missing_file["outputPath"]
        queue_missing_files_paths.append(path.removeprefix(arr_target_folder))

    logging.info(
        "Done, discovered "
        + str(len(queue_missing_files_paths))
        + " missing elements."
    )
    return queue_missing_files_paths
