import asyncio
import argparse

from run import run

async def main():
    parser = argparse.ArgumentParser(
        prog="nextcloud-downloads-importer",
        description="Retrieve from a Nextcloud instance files that a Sonarr/Radarr instance asked to be downloaded.",
    )

    parser.add_argument("target_folder")
    parser.add_argument("--arr_api_url", required=True)
    parser.add_argument("--arr_api_key", required=True)
    parser.add_argument("--arr_target_dir", required=True)
    parser.add_argument("--nextcloud_url", required=True)
    parser.add_argument("--nextcloud_user", required=True)
    parser.add_argument("--nextcloud_password", required=True)
    parser.add_argument("--nextcloud_download_folder", required=True)
    parser.add_argument("--periodicity", type=int, required=False, default=1800)

    args = parser.parse_args()

    await run(
        args.arr_api_url,
        args.arr_api_key,
        args.arr_target_dir,
        args.nextcloud_url,
        args.nextcloud_user,
        args.nextcloud_password,
        args.nextcloud_download_folder,
        args.target_folder,
        args.periodicity
    )


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
