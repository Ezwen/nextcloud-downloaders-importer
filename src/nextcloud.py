import nc_py_api
import pathlib
import logging


def _download_file(
    nc: nc_py_api.Nextcloud, nc_file: nc_py_api.FsNode, target_folder: pathlib.Path
):
    logging.info(
        "Starting download of Nextcloud file "
        + nc_file.user_path
        + " into "
        + str(target_folder.absolute())
    )
    target_folder.mkdir(parents=True, exist_ok=True)
    target_file = target_folder / nc_file.name

    if target_file.exists():
        if target_file.stat().st_size == nc_file.info.content_length:
            logging.info("File already there, skipping download.")
            return
        else:
            logging.info(
                "File already partially downloaded, removing and starting download from start."
            )
            target_file.unlink()

    target_file_part = pathlib.Path(str(target_file.absolute()) + ".part")
    if target_file_part.exists():
        target_file_part.unlink()
    target_file_part.touch()
    with target_file_part.open("wb") as target_file_part_stream:
        nc.files.download2stream(nc_file, target_file_part_stream)
        logging.info("Download finished.")

    target_file_part.rename(target_file)


def _download_file_or_folder(
    nc: nc_py_api.Nextcloud,
    nc_file_or_folder: nc_py_api.FsNode,
    target_folder: pathlib.Path,
):
    if nc_file_or_folder.is_dir:
        for file in nc.files.listdir(nc_file_or_folder.user_path):
            _download_file_or_folder(nc, file, target_folder / nc_file_or_folder.name)
    else:
        _download_file(nc, nc_file_or_folder, target_folder)


def download_path(
    nc: nc_py_api.Nextcloud,
    nc_downloads_root: str,
    nc_path_from_root: str,
    target_dir: str,
):
    nc_relative_path: str = nc_downloads_root + "/" + nc_path_from_root
    nc_relative_path_parent : str = nc_relative_path + "/.."
    logging.info("Processing download of Nextcloud element " + nc_relative_path)
    all_root_files: list[nc_py_api.FsNode] = nc.files.listdir(nc_relative_path_parent)

    nc_file = [
        file
        for file in all_root_files
        if file.user_path == nc_relative_path + ("/" if file.is_dir else "")
    ][0]

    real_target_dir : str = target_dir + "/" + nc_relative_path_parent.removeprefix(nc_downloads_root)

    _download_file_or_folder(nc, nc_file, pathlib.Path(real_target_dir).resolve())
    logging.info("Done, element " + nc_relative_path + "fully downloaded.")


def connect(
    nextcloud_url: str, nextcloud_user: str, nextcloud_pass: str
) -> nc_py_api.Nextcloud:
    logging.info(
        "Preparing connection to Nextcloud instance "
        + nextcloud_url
        + " as user "
        + nextcloud_user
    )
    return nc_py_api.Nextcloud(
        nextcloud_url=nextcloud_url,
        nc_auth_user=nextcloud_user,
        nc_auth_pass=nextcloud_pass,
        npa_nc_cert=False,
    )
