import logging
import nextcloud
import arr
import time

API_TIMEOUT = 10

logging.basicConfig(
    format="%(asctime)s [%(levelname)s]: %(message)s",
    level=logging.INFO,
    handlers=[logging.StreamHandler()],
)

async def run_once(
    arr_api_url: str,
    arr_api_key: str,
    arr_target_dir: str,
    nextcloud_url: str,
    nextcloud_user: str,
    nextcloud_password: str,
    nextcloud_download_folder: str,
    target_folder: str,
):
    logging.info("Starting")

    missing_elements_paths = await arr.get_missing_elements_paths(
        arr_api_url, arr_api_key, arr_target_dir
    )

    nc = nextcloud.connect(nextcloud_url, nextcloud_user, nextcloud_password)

    for missing_element_path in missing_elements_paths:
        nextcloud.download_path(
            nc, nextcloud_download_folder, missing_element_path, target_folder
        )

    logging.info("Closing")


async def run(
    arr_api_url: str,
    arr_api_key: str,
    arr_target_dir: str,
    nextcloud_url: str,
    nextcloud_user: str,
    nextcloud_password: str,
    nextcloud_download_folder: str,
    target_folder: str,
    periodicity: int,
):
    while True:
        try:
            await run_once(
                arr_api_url,
                arr_api_key,
                arr_target_dir,
                nextcloud_url,
                nextcloud_user,
                nextcloud_password,
                nextcloud_download_folder,
                target_folder,
            )
        except Exception as inst:
            logging.error("An error occured:")
            logging.error(inst)
            
        logging.info(f"Sleeping {periodicity} seconds until next run.")
        time.sleep(periodicity)
